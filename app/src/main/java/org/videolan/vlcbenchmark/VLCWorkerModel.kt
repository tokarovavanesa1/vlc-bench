/*
 *****************************************************************************
 * VLCWorkerModel.kt
 *****************************************************************************
 * Copyright © 2016-2021 VLC authors and VideoLAN
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/
package org.videolan.vlcbenchmark

import android.content.*
import android.content.pm.ActivityInfo
import android.media.projection.MediaProjectionManager
import android.os.Bundle
import android.os.Handler
import android.os.IBinder
import android.util.DisplayMetrics
import android.util.Log
import android.view.View
import androidx.annotation.UiThread
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.json.JSONException
import org.videolan.vlcbenchmark.Constants.TestType
import org.videolan.vlcbenchmark.api.RetrofitInstance.Companion.init
import org.videolan.vlcbenchmark.benchmark.BenchmarkActivityState
import org.videolan.vlcbenchmark.benchmark.BenchmarkViewModel
import org.videolan.vlcbenchmark.benchmark.BenchmarkService
import org.videolan.vlcbenchmark.benchmark.BenchmarkService.ScreenshotServiceBinder
import org.videolan.vlcbenchmark.benchmark.ScreenshotInitVariables
import org.videolan.vlcbenchmark.results.ResultController
import org.videolan.vlcbenchmark.results.ResultController.Companion.mergeResults
import org.videolan.vlcbenchmark.results.ResultParser
import org.videolan.vlcbenchmark.results.ResultRepository
import org.videolan.vlcbenchmark.tests.TestController
import org.videolan.vlcbenchmark.tests.TestRepository.getTestList
import org.videolan.vlcbenchmark.tests.types.TestPlayback
import org.videolan.vlcbenchmark.tests.types.TestQuality
import org.videolan.vlcbenchmark.tests.types.TestSpeed
import org.videolan.vlcbenchmark.tools.GoogleConnectionHandler.Companion.instance
import org.videolan.vlcbenchmark.tools.StorageManager
import org.videolan.vlcbenchmark.tools.StorageManager.setStoragePreference
import org.videolan.vlcbenchmark.tools.Util.errorSnackbar
import org.videolan.vlcbenchmark.tools.Util.runInBackground
import org.videolan.vlcbenchmark.tools.Util.runInUiThread
import java.io.IOException

/**
 *
 *
 * Main class of the project.
 * This class handle the whole logic/algorithm side of the application.
 *
 * It extends from Activity yet it doesn't touch anything related to UI except [VLCWorkerModel.onActivityResult]
 * and at some specific points the termination of the activity.
 *
 * This class cannot be instantiated directly it requires to be extended and to implement all of its abstract method,
 * its in those methods only that the UI part of the activity can be handled.
 *
 * This architecture allows the UI and logical part to be independent from one an other.
 */
private const val STATE_RUNNING = "STATE_RUNNING"
private const val STATE_STATE = "STATE_STATE"
private const val STATE_DUMMY = "STATE_DUMMY"
private const val STATE_TEST_CONTROLLER = "STATE_TEST_CONTROLLER"
private const val STATE_RESULT_CONTROLLER = "STATE_RESULT_CONTROLLER"
abstract class VLCWorkerModel : AppCompatActivity() {
    private var data: Intent? = null
    private var resultCode = -2
    protected lateinit var model: BenchmarkViewModel
    protected lateinit var view: View
    var service: BenchmarkService? = null
    var bound = false

    protected abstract fun fakeInput()
    protected abstract fun askUploadPermission()
    protected abstract fun checkFiles()

    /**
     * Called to update the benchmark progress view.
     * @param progress benchmark progress percentage
     * @param progressText text recaping the progress state of the benchmark:
     * file index, loop number, etc
     * @param sampleName the name of the test (ex : screenshot software, ...)
     */
    protected abstract fun updateBenchmarkProgress(progress: Double, progressPercent: String, progressText: String, sampleName: String)

    override fun onStart() {
        super.onStart()
        bindToService()
    }

    protected fun bindToService() {
        if (!model.finished && !bound) {
            val intent = Intent(this, BenchmarkService::class.java)
            startService(intent)
            val boundIntent = Intent(this, BenchmarkService::class.java)
            bindService(boundIntent, connection, BIND_AUTO_CREATE)
        }
    }

    private fun unbindFromService() {
        if (bound) {
            unbindService(connection)
            bound = false
        }
    }

    protected fun stopService() {
        service?.let {
            it.stopSelf()
            unbindFromService()
        }
    }

    override fun onStop() {
        super.onStop()
        unbindFromService()
    }

    private val connection: ServiceConnection = object : ServiceConnection {
        override fun onServiceConnected(componentName: ComponentName, iBinder: IBinder) {
            val binder = iBinder as ScreenshotServiceBinder
            service = binder.getService()
            bound = true
        }

        override fun onServiceDisconnected(componentName: ComponentName) {
            bound = false
        }
    }

    /**
     * Initialization of the Activity.
     * request permissions to read on the external storage.
     *
     * @param savedInstanceState saved state bundle
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        model = ViewModelProviders.of(this).get(BenchmarkViewModel::class.java)
        setStoragePreference(this)
        init(getString(R.string.build_api_address))
        instance
    }

    @UiThread
    protected open fun startBenchmark() {
        model.running = true
        model.configVersion = model.testController.version
        bindToService()
        setupScreenshotCallbacks()
    }

    private fun setupScreenshotCallbacks() {
        model.projectionManager = getSystemService(MEDIA_PROJECTION_SERVICE) as MediaProjectionManager
        if (model.projectionManager == null) {
            Log.e(TAG, "launchTests: Failed to create MediaProjectionManager")
            errorSnackbar(view, R.string.snack_error_screenshot_preparation)
            model.setState(BenchmarkActivityState.ERROR)
            stopBenchmark()
            return
        }
        model.projectionManager?.let {
            val intent = Intent(this, BenchmarkService::class.java)
            startService(intent)
            val boundIntent = Intent(this, BenchmarkService::class.java)
            bindService(boundIntent, connection, BIND_AUTO_CREATE)
            val screenshotIntent = it.createScreenCaptureIntent()
            screenshotIntent.flags = Intent.FLAG_ACTIVITY_NO_ANIMATION and Intent.FLAG_ACTIVITY_PREVIOUS_IS_TOP
            startActivityForResult(screenshotIntent, Constants.RequestCodes.SCREENSHOT)
        }
    }

    /**
     * This method creates a new intent that corresponds with VLC's BenchActivity launch protocol.
     * Then calls the listener method to continue the benchmark flow
     */
    private fun createIntentForVlc(listener: OnIntentCreatedListener) {
        val test = model.testController.getCurrentTest()
        Log.i(TAG, "createIntentForVlc: running ${test.getPrettyTypeString()} test")
        when (test.type) {
            TestType.PLAYBACK -> (test as TestPlayback).prepareIntent(this, Intent(), listener)
            TestType.QUALITY -> (test as TestQuality).prepareIntent(this, Intent(), listener)
            TestType.SPEED -> (test as TestSpeed).prepareIntent(this, Intent(), listener)
            TestType.UNKNOWN -> {
            }
        }
    }

    interface OnIntentCreatedListener {
        fun onIntentCreated(intent: Intent?)
    }

    /**
     * Re-entry point, in this method we receive directly from VLC its
     * result code along with an Intent giving extra information about the result of VLC.
     *
     * This method will be called a each end of a test and will therefor handle the launch of the next tests.
     * It also handle that case were crashed without notice by checking if the Intent in argument if null and if so
     * by getting the String describing the crashed VLC had by reading into VLC's shared preferences.
     *
     * This method also calls a number of abstract method to allow the UI to update itself.
     *
     * @param requestCode the code we gave to VLC to launch itself.
     * @param resultCode  the code on which VLC finished.
     * @param data an Intent describing additional information and data about the test.
     */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) { //TODO refactor all this, lots of useless stuff
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Constants.RequestCodes.VLC) {
            Log.i(TAG, "onActivityResult: resultCode: $resultCode")
            this.data = data
            this.resultCode = resultCode
        } else if (requestCode == Constants.RequestCodes.GOOGLE_CONNECTION) {
            val fragment = supportFragmentManager.findFragmentById(R.id.main_page_fragment_holder)
            fragment?.onActivityResult(requestCode, resultCode, data)
                    ?: Log.e(TAG, "onActivityResult: GOOGLE_CONNECTION: fragment is null")
        } else if (data != null && requestCode == Constants.RequestCodes.SCREENSHOT && resultCode == RESULT_OK) {
            model.projectionManager?.let {
                val metrics = DisplayMetrics()
                windowManager.defaultDisplay.getRealMetrics(metrics)
                val mediaProjection = it.getMediaProjection(resultCode, data)
                if (mediaProjection == null) {
                    Log.e(TAG, "onActivityResult: Failed to create MediaProjection")
                    errorSnackbar(view, R.string.snack_error_screenshot_projection)
                    model.setState(BenchmarkActivityState.ERROR)
                    return
                }
                if (bound) {
                    // Most of the time when setting up screen dimensions, the orientation will be
                    // portrait mode, but the screenshots are taken in landscape mode.
                    // It's important to invert dimension values when in portrait mode.
                    ScreenshotInitVariables.dpi = metrics.densityDpi
                    ScreenshotInitVariables.mediaProjection = mediaProjection
                    if (metrics.widthPixels < 1 || metrics.heightPixels < 1) {
                        Log.e(TAG, "onActivityResult: Failed to get positive image dimensions")
                        errorSnackbar(view, R.string.snack_error_invalid_image_dimensions)
                        model.setState(BenchmarkActivityState.ERROR)
                        stopBenchmark()
                        return
                    }
                    if (metrics.widthPixels >= metrics.heightPixels) {
                        ScreenshotInitVariables.width = metrics.widthPixels
                        ScreenshotInitVariables.height = metrics.heightPixels
                        service?.setupBenchmark()
                    } else {
                        ScreenshotInitVariables.width = metrics.heightPixels
                        ScreenshotInitVariables.height = metrics.widthPixels
                        service?.setupBenchmark()
                    }
                } else {
                    Log.e(TAG, "onActivityResult: Service not bound")
                    model.setState(BenchmarkActivityState.ERROR)
                    stopBenchmark()
                }
                try {
                    createIntentForVlc(object : OnIntentCreatedListener {
                        override fun onIntentCreated(intent: Intent?) {
                            /* In case of failure due to an invalid file, stop benchmark, and display download page */if (intent == null) {
                                errorSnackbar(view, R.string.snack_error_failed_start_vlc)
                                stopBenchmark()
                                finish()
                            }
                            requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE
                            if (intent != null) {
                                startActivityForResult(intent, Constants.RequestCodes.VLC)
                            } else {
                                errorSnackbar(view, R.string.snack_error_invalid_file)
                                model.running = false
                                checkFiles()
                            }
                        }
                    })
                } catch (e: ActivityNotFoundException) {
                    errorSnackbar(view, R.string.dialog_text_vlc_failed)
                    Log.e(TAG, "launchTests: Failed to start VLC")
                    stopBenchmark()
                    model.setState(BenchmarkActivityState.ERROR)
                }
            }
        } else if (requestCode == Constants.RequestCodes.SCREENSHOT) {
            errorSnackbar(view, R.string.snack_error_vlc_screenshot)
            stopBenchmark()
            model.setState(BenchmarkActivityState.ERROR)
        }
    }

    private fun updateProgress() {
        updateBenchmarkProgress(model.testController.getProgress(model.resultController.currentLoopIndex,
                model.resultController.maxLoop),
                model.testController.getProgressPercentString(this,
                        model.resultController.currentLoopIndex,
                        model.resultController.maxLoop),
                model.testController.getProgressString(this,
                        model.resultController.currentLoopIndex,
                        model.resultController.maxLoop),
                model.testController.getCurrentTest().sample.name)
    }

    /**
     * This method increment all the counters relatives to the type of test we will do,
     * such as the type of test we should do, the index of the file we're testing and
     * on what loop are we.
     *
     *
     * If we reached the end of the tests we then calculate the average score for hardware and software
     * call the abstract method [VLCWorkerModel.onTestsFinished]  and return
     *
     *
     * Otherwise we launch VLC's BenchActivity with the counters' new values.
     */
    private fun launchNextTest() {
        if (model.running) {
            fakeInput()
            if (model.dummySample) {
                if (!model.testController.next()) {
                    if (model.resultController.isLastLoop()) {
                        onTestsFinished()
                        return
                    } else {
                        model.testController.reset()
                        model.resultController.nextLoop()
                    }
                }
            } else {
                model.dummySample = true
            }
            model.goingToNextTest = false
            updateProgress()
            createIntentForVlc(object : OnIntentCreatedListener {
                override fun onIntentCreated(intent: Intent?) {
                    if (intent == null) {
                        errorSnackbar(view, R.string.snack_error_failed_start_vlc)
                        stopBenchmark()
                        model.setState(BenchmarkActivityState.ERROR)
                    }
                    // Add delay for vlc to finish correctly
                    val handler = Handler()
                    handler.postDelayed({
                        if (model.running) {
                            requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE
                            startActivityForResult(intent, Constants.RequestCodes.VLC)
                        }
                    }, 4000)
                }
            })
        } else {
            Log.e(TAG, "launchNextTest was called but running is false.")
        }
    }

    private fun onTestsFinished() {
        val resultRepository = ResultRepository()
        val results = resultRepository.getCurrentResultList(this)
        if (results.isEmpty()) {
            Log.e(TAG, "onTestsFinished: Failed to get results")
            model.setState(BenchmarkActivityState.ERROR)
            errorSnackbar(view, R.string.dialog_text_saving_results_failure)
            return
        }
        val sanitizedResults = mergeResults(results)
        model.finished = true
        model.goingToNextTest = false
        resultRepository.discardCurrentResultList(this)
        stopBenchmark()
        if (sanitizedResults == null) {
            Log.e(TAG, "onTestsFinished: Failed to save benchmark results")
            model.setState(BenchmarkActivityState.ERROR)
            errorSnackbar(view, R.string.dialog_text_saving_results_failure)
            return
        }
        model.finalResults = sanitizedResults
        runInBackground(Runnable {
            var savedName: String? = null
            try {
                savedName = resultRepository.saveResultList(sanitizedResults)
            } catch (e: JSONException) {
                Log.e(TAG, "Failed to save test : $e")
                errorSnackbar(view, R.string.dialog_text_saving_results_failure)
                model.setState(BenchmarkActivityState.ERROR)
                return@Runnable
            }
            model.resultName = savedName
            runInUiThread(Runnable { askUploadPermission() })
        })
    }

    protected fun stopBenchmark() {
        stopService()
        model.running = false
        ScreenshotInitVariables.mediaProjection = null
        ScreenshotInitVariables.dpi = 0
        ScreenshotInitVariables.height = 0
        ScreenshotInitVariables.width = 0
    }

    override fun onResume() {
        super.onResume()
        view = findViewById(R.id.scrollview)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR
        if (model.running) {

            // -2 isn't return by vlc-android
            // small hack to stop the benchmark from restarting vlc if there is a
            // configuration change between onResume and vlc starting
            if (resultCode != -2 && model.dummySample) {
                runInBackground(Runnable {
                    try {
                        model.testController.setTestList(getTestList(this))
                        model.goingToNextTest = true
                        runInUiThread(Runnable { updateProgress() })
                    } catch (exception: IOException) {
                        runInUiThread(Runnable {
                            errorSnackbar(view, R.string.toast_error_fail_load_config)
                            Log.e(TAG, "onResume: $exception")
                            stopBenchmark()
                            model.setState(BenchmarkActivityState.ERROR)
                        })
                        return@Runnable
                    }
                    val result = ResultParser().parse(this, data, resultCode, model.testController)
                    if (result == null) {
                        errorSnackbar(view, R.string.toast_error_result_parse)
                        stopBenchmark()
                        model.setState(BenchmarkActivityState.ERROR)
                        return@Runnable
                    }
                    model.resultController.addResults(this, result)
                    resultCode = -2
                    runInUiThread(Runnable { launchNextTest() })
                })
            } else if (resultCode != -2 && !model.dummySample) {
                val context = this
                StorageManager.deleteScreenshots()
                lifecycleScope.launch(Dispatchers.IO) {
                    try {
                        model.testController.setTestList(getTestList(context))
                    } catch (exception: IOException) {
                        lifecycleScope.launch(Dispatchers.Main) {
                            errorSnackbar(view, R.string.toast_error_fail_load_config)
                            Log.e("VLCWorkerModel", "onResume: $exception")
                            stopBenchmark()
                            model.setState(BenchmarkActivityState.ERROR)
                        }
                    }
                    resultCode = -2
                    lifecycleScope.launch(Dispatchers.Main) { launchNextTest() }
                }
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putBoolean(STATE_RUNNING, model.running)
        outState.putBoolean(STATE_DUMMY, model.dummySample)
        outState.putSerializable(STATE_STATE, model.state.value)
        if (model.running && !model.goingToNextTest) {
            model.testController.dropTestList()
            outState.putParcelable(STATE_TEST_CONTROLLER, model.testController)
            outState.putParcelable(STATE_RESULT_CONTROLLER, model.resultController)
        }
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        val testController: TestController?
        val resultController: ResultController?
        model.running = savedInstanceState.getBoolean(STATE_RUNNING)
        model.dummySample = savedInstanceState.getBoolean(STATE_DUMMY)
        val state = savedInstanceState.getSerializable(STATE_STATE) as BenchmarkActivityState?
        if (state != null)
            model.setState(state)
        if (model.running && !model.goingToNextTest) {
            testController = savedInstanceState.getParcelable(STATE_TEST_CONTROLLER)
            if (testController != null) model.testController = testController
            resultController = savedInstanceState.getParcelable(STATE_RESULT_CONTROLLER)
            if (resultController != null) model.resultController = resultController
        }
    }

    companion object {
        private const val TAG = "VLCWorkerModel"
        const val SHARED_PREFERENCE = "org.videolab.vlc.gui.video.benchmark.UNCAUGHT_EXCEPTIONS"
        const val SHARED_PREFERENCE_STACK_TRACE = "org.videolab.vlc.gui.video.benchmark.STACK_TRACE"
    }
}