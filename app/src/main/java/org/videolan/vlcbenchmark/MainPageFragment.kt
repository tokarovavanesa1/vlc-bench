/*
 *****************************************************************************
 * MainPageFragment.kt
 *****************************************************************************
 * Copyright © 2016-2021 VLC authors and VideoLAN
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/
package org.videolan.vlcbenchmark

import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.ActivityInfo
import android.net.Uri
import android.os.BatteryManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ScrollView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.videolan.vlcbenchmark.tools.SystemPropertiesProxy.cpuMaxFreq
import org.videolan.vlcbenchmark.tools.SystemPropertiesProxy.cpuMinFreq
import org.videolan.vlcbenchmark.tools.SystemPropertiesProxy.cpuModel
import org.videolan.vlcbenchmark.tools.SystemPropertiesProxy.getResolution
import org.videolan.vlcbenchmark.tools.SystemPropertiesProxy.ramTotal
import org.videolan.vlcbenchmark.benchmark.BenchmarkStepper
import org.videolan.vlcbenchmark.tools.DialogInstance
import org.videolan.vlcbenchmark.tools.FormatStr.byteSizeToString
import org.videolan.vlcbenchmark.tools.SystemPropertiesProxy
import org.videolan.vlcbenchmark.tools.vlc.VLCProxy
import org.videolan.vlcbenchmark.tools.vlc.VLCProxy.checkSignature
import org.videolan.vlcbenchmark.tools.vlc.VLCProxy.checkVlcVersion
import kotlin.math.roundToInt

/**
 * Fragment where the user can start the benchmark
 * When started VLCBenchmark will check:
 * - If the user has VLC
 * - If the user has the right version
 * - Check the battery level
 * - Check file integrity / download if missing
 * - Check the presence of a previous benchmark that was interrupted
 */
class MainPageFragment : Fragment() {
    private var testNumber = 0
    private var model: TextView? = null

    private fun redirectToVlcStore(beta: Boolean) {
        if (!beta) {
            try {
                val uri = Uri.parse("market://detail?id=org.videolan.vlc")
                startActivity(Intent(Intent.ACTION_VIEW, uri))
            } catch (e: Exception) {
                try {
                    Log.w(TAG, "redirectToVlcStore: $e")
                    val uri = Uri.parse("https://play.google.com/store/apps/details?id=org.videolan.vlc")
                    startActivity(Intent(Intent.ACTION_VIEW, uri))
                } catch (e: Exception) {
                    Snackbar.make(requireView(), R.string.snack_error_no_browser_googleplay, Snackbar.LENGTH_LONG).show()
                }
            }
        } else {
            try {
                val uri = Uri.parse("https://play.google.com/apps/testing/org.videolan.vlc")
                startActivity(Intent(Intent.ACTION_VIEW, uri))
            } catch (e: Exception) {
                Snackbar.make(requireView(), R.string.snack_error_no_browser_googleplay, Snackbar.LENGTH_LONG).show()
            }
        }
    }

    private fun checkForVLC() {
        if (activity == null) {
            Log.e(TAG, "checkForVLC: null context")
            return
        }
        val vlcSignature = checkSignature(requireActivity())
        val vlcVersion = checkVlcVersion(requireActivity())
        if (!vlcSignature || !vlcVersion) {
            if (!vlcSignature) {
                Log.e(TAG, "Could not find VLC Media Player")
            } else {
                Log.e(TAG, "Outdated VLC Media Player")
            }
            val beta = VLCProxy.isCurrentVLCBeta(requireActivity())
            val msg = if (beta) {
                String.format(resources.getString(R.string.dialog_text_missing_vlc_beta), BuildConfig.VLC_VERSION)
            } else {
                String.format(resources.getString(R.string.dialog_text_missing_vlc), BuildConfig.VLC_VERSION)
            }
            requireActivity().requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LOCKED
            val dialog = AlertDialog.Builder(context)
                    .setTitle(resources.getString(R.string.dialog_title_missing_vlc))
                    .setMessage(msg)
                    .setNeutralButton(resources.getString(R.string.dialog_btn_cancel)) { _: DialogInterface?, _: Int ->
                        requireActivity().requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_FULL_USER
                    }
                    .setNegativeButton(resources.getString(R.string.dialog_btn_continue)
                    ) { _: DialogInterface?, _: Int ->
                        requireActivity().requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_FULL_USER
                        redirectToVlcStore(beta)
                    }
            if (beta)
                dialog.setView(R.layout.dialog_qrcode)
            dialog.show()
            return
        }
        checkBattery()
    }

    private fun checkBattery() {
        if (context == null) {
            Log.e(TAG, "checkBattery: null context")
            return
        }
        val intentFilter = IntentFilter(Intent.ACTION_BATTERY_CHANGED)
        val batteryStatus = requireContext().registerReceiver(null, intentFilter)
        if (batteryStatus == null) {
            Log.e(TAG, "checkForTestStart: battery intent is null")
            DialogInstance(R.string.dialog_title_oups, R.string.dialog_text_oups).display(activity)
            return
        }
        val status = batteryStatus.getIntExtra(BatteryManager.EXTRA_STATUS, -1)
        val isCharging = status == BatteryManager.BATTERY_STATUS_CHARGING ||
                status == BatteryManager.BATTERY_STATUS_FULL
        val level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1)
        val scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1)
        val batteryPct = level / scale.toFloat() * 100f
        if (batteryPct <= 50f && !isCharging) {
            AlertDialog.Builder(context)
                    .setTitle(resources.getString(R.string.dialog_title_warning))
                    .setMessage(String.format(resources.getString(R.string.dialog_text_battery_warning), batteryPct.roundToInt()))
                    .setNeutralButton(resources.getString(R.string.dialog_btn_cancel), null)
                    .setNegativeButton(resources.getString(R.string.dialog_btn_continue)) { _, _ -> startBenchmarkActivity() }
                    .show()
        } else {
            startBenchmarkActivity()
        }
    }

    fun setDeviceName(modelName: String) {
        model?.text = modelName
    }

    private fun fillDeviceLayout(view: View) {
        val android = view.findViewById<TextView>(R.id.specs_android_text)
        val cpu = view.findViewById<TextView>(R.id.specs_cpu_text)
        val cpuSpeed = view.findViewById<TextView>(R.id.specs_cpuspeed_text)
        val memory = view.findViewById<TextView>(R.id.specs_memory_text)
        val resolution = view.findViewById<TextView>(R.id.specs_resolution_text)
        val freeSpace = view.findViewById<TextView>(R.id.specs_free_space_text)
        model = view.findViewById(R.id.specs_model_text)

        setDeviceName(Build.MODEL)
        val context = requireActivity()
        lifecycleScope.launch(Dispatchers.IO) {
            val properName = SystemPropertiesProxy.getProperDeviceName(context)
            withContext(Dispatchers.Main) {
                setDeviceName(properName)
            }
        }

        android.text = Build.VERSION.RELEASE
        cpu.text = cpuModel
        cpuSpeed.text = String.format(getString(R.string.specs_cpu_speed_value), cpuMinFreq, cpuMaxFreq)
        memory.text = ramTotal
        if (activity != null) resolution.text = getResolution(requireActivity()) else Log.e(TAG, "fillDeviceLayout: null activity")
        if (context != null) freeSpace.text = byteSizeToString(requireContext(),
                SystemPropertiesProxy.freeSpace) else Log.e(TAG, "fillDeviceLayout: null context")
    }

    private fun startBenchmarkActivity() {
        val intent = Intent(activity, BenchmarkStepper::class.java)
        intent.putExtra("testNumber", testNumber)
        startActivity(intent)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_main_page, container, false)
        val oneTest: FloatingActionButton = view.findViewById(R.id.fab_test_x1)
        oneTest.setOnClickListener {
            testNumber = 1
            checkForVLC()
        }
        oneTest.setOnLongClickListener {
            testNumber = 3
            checkForVLC()
            true
        }
        val specs = view.findViewById<ScrollView>(R.id.specs_scrollview)
        specs.isFocusable = false
        val explanations = view.findViewById<ScrollView>(R.id.test_explanation_scrollview)
        explanations.isFocusable = false
        fillDeviceLayout(view)
        return view
    }

    companion object {
        private val TAG = MainPageFragment::class.java.name
    }
}