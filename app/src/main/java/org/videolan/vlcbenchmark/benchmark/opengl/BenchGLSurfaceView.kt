/*
 *****************************************************************************
 * BenchGLSurfaceView.kt
 *****************************************************************************
 * Copyright © 2017 - 2021 VLC authors and VideoLAN
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/
package org.videolan.vlcbenchmark.benchmark.opengl

import android.content.Context
import android.opengl.GLSurfaceView
import javax.microedition.khronos.egl.EGLConfig
import javax.microedition.khronos.opengles.GL10

/**
 * This class allows us to get a handle on the GL context
 * and get gpu information
 */
class BenchGLSurfaceView(private val glActivityContext: Context) : GLSurfaceView(glActivityContext) {
    /* Gpu information*/
    var glRenderer: String? = null
        private set
    var glVendor: String? = null
        private set
    var glVersion: String? = null
        private set
    var glExtension: String? = null
        private set

    private inner class BenchGLRenderer : Renderer {
        override fun onSurfaceCreated(gl: GL10, config: EGLConfig) {
            glRenderer = gl.glGetString(GL10.GL_RENDERER)
            glVendor = gl.glGetString(GL10.GL_VENDOR)
            glVersion = gl.glGetString(GL10.GL_VERSION)
            glExtension = gl.glGetString(GL10.GL_EXTENSIONS)
            (glActivityContext as BenchGLActivity).sendGlInfo()
        }

        override fun onSurfaceChanged(gl: GL10, width: Int, height: Int) {}
        override fun onDrawFrame(gl: GL10) {}
    }

    init {
        setEGLContextClientVersion(2)
        val renderer = BenchGLRenderer()
        setRenderer(renderer)
    }
}