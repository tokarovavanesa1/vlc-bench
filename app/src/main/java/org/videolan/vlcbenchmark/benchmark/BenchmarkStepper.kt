/*
 *****************************************************************************
 * BenchmarkStepper.kt
 *****************************************************************************
 * Copyright © 2016-2018 VLC authors and VideoLAN
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

package org.videolan.vlcbenchmark.benchmark

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.PersistableBundle
import android.util.Log
import android.view.KeyEvent
import android.view.MenuItem
import android.view.View
import androidx.appcompat.widget.Toolbar
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import kotlinx.android.synthetic.main.benchmark_stepper.*
import kotlinx.android.synthetic.main.benchmark_stepper.main_toolbar
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.videolan.vlcbenchmark.*
import org.videolan.vlcbenchmark.benchmark.opengl.BenchGLActivity
import org.videolan.vlcbenchmark.results.ResultController
import org.videolan.vlcbenchmark.results.ResultModel
import org.videolan.vlcbenchmark.results.ResultRepository
import org.videolan.vlcbenchmark.tests.TestRepository
import org.videolan.vlcbenchmark.tools.*
import org.videolan.vlcbenchmark.tools.FormatStr.byteSizeToString
import org.videolan.vlcbenchmark.tools.Util.errorSnackbar

private const val STATE_VALUE: String = "STATE_VALUE"

class BenchmarkStepper : VLCWorkerModel() {

    private var loopNumber = 0
    private var keyEvent: KeyEvent? = null

    private val br = BenchmarkStepperBroadcastReceiver()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.benchmark_stepper)
        (main_toolbar as Toolbar).title = getString(R.string.app_name)
        setSupportActionBar(main_toolbar as Toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        loopNumber = intent.getIntExtra(Constants.EXTRA_BENCHMARK_TEST_NUMBER, 0)
        if (loopNumber != 1 && loopNumber != 3) {
            finish()
        }
        setupUiStates()
        resetUiStates()
    }

    private fun resetUiStates() {
        loading_collapsible.visibility = View.GONE
        no_google_collapsible.visibility = View.GONE
        error_collapsible.visibility = View.GONE
        download_notice_collapsible.visibility = View.GONE
        download_collapsible.visibility = View.GONE
        previous_bench_collapsible.visibility = View.GONE
        benchmark_notice_collapsible.visibility = View.GONE
        benchmark_collapsible.visibility = View.GONE
        upload_notice_collapsible.visibility = View.GONE
        upload_notice_failure.visibility = View.GONE
        upload_progress.visibility = View.GONE
        upload_success_collapsible.visibility = View.GONE
        delete_samples_collapsible.visibility = View.GONE
        download_title.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))
        download_circle.background = getDrawable(R.drawable.circle)
        benchmark_title.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))
        benchmark_circle.background = getDrawable(R.drawable.circle)
        upload_title.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))
        upload_circle.background = getDrawable(R.drawable.circle)
    }

    private fun disableDownloadSection() {
        download_title.setTextColor(ResourcesCompat.getColor(resources, R.color.grey500, null))
        download_circle.background = getDrawable(R.drawable.circle_grey)
    }

    private fun disableBenchmarkSection() {
        benchmark_title.setTextColor(ResourcesCompat.getColor(resources, R.color.grey500, null))
        benchmark_circle.background = getDrawable(R.drawable.circle_grey)
    }

    private fun disableUploadSection() {
        upload_title.setTextColor(ResourcesCompat.getColor(resources, R.color.grey500, null))
        upload_circle.background = getDrawable(R.drawable.circle_grey)
    }

    private fun disableUploadButtons() {
        upload_notice_btn_yes.setTextColor(ResourcesCompat.getColor(resources, R.color.grey500, null))
        upload_notice_btn_no.setTextColor(ResourcesCompat.getColor(resources, R.color.grey500, null))
        upload_notice_btn_yes.isEnabled = false
        upload_notice_btn_no.isEnabled = false
    }

    private fun enableUploadButtons() {
        upload_notice_btn_yes.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))
        upload_notice_btn_no.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))
        upload_notice_btn_yes.isEnabled = true
        upload_notice_btn_no.isEnabled = true
    }

    private fun setupUiStates() {
        model.state.observe(this, Observer { state ->
            if (state != null) {
                when (state) {
                    BenchmarkActivityState.NO_GOOGLE -> {
                        resetUiStates()
                        disableBenchmarkSection()
                        disableUploadSection()
                        no_google_collapsible.visibility = View.VISIBLE
                        no_google_btn_continue.setOnClickListener {
                            model.setState(BenchmarkActivityState.LOADING)
                            checkFiles()
                        }
                        no_google_btn_cancel.setOnClickListener {
                            stopService()
                            finish()
                        }
                    }
                    BenchmarkActivityState.LOADING -> {
                        resetUiStates()
                        disableBenchmarkSection()
                        disableUploadSection()
                    }
                    BenchmarkActivityState.NO_SAMPLES -> {
                        resetUiStates()
                        error_collapsible.visibility = View.VISIBLE
                        error_text.text = getString(R.string.stepper_no_samples_text)
                        error_icon.setImageResource(R.drawable.ic_baseline_cloud_off_24)
                        disableBenchmarkSection()
                        disableUploadSection()
                        error_btn_try_again.setOnClickListener {
                            checkFiles()
                        }
                        error_btn_cancel.setOnClickListener {
                            stopService()
                            finish()
                        }
                    }
                    BenchmarkActivityState.NO_SPACE -> {
                        resetUiStates()
                        error_collapsible.visibility = View.VISIBLE
                        error_text.text = String.format(getString(R.string.stepper_no_space_text),
                            byteSizeToString(this, model.spaceMissing))
                        disableBenchmarkSection()
                        disableUploadSection()
                        error_btn_try_again.setOnClickListener {
                            checkFiles()
                        }
                        error_btn_cancel.setOnClickListener {
                            stopService()
                            finish()
                        }
                    }
                    BenchmarkActivityState.ERROR -> {
                        resetUiStates()
                        error_collapsible.visibility = View.VISIBLE
                        error_text.text = getString(R.string.stepper_error)
                        disableBenchmarkSection()
                        disableUploadSection()
                        error_btn_try_again.setOnClickListener {
                            bindToService()
                            checkFiles()
                        }
                        error_btn_cancel.setOnClickListener {
                            stopService()
                            finish()
                        }
                    }
                    BenchmarkActivityState.DOWNLOAD_NOTICE -> {
                        resetUiStates()
                        download_notice_collapsible.visibility = View.VISIBLE
                        disableBenchmarkSection()
                        disableUploadSection()
                        val message = if (!Util.hasWifiAndLan(this)) {
                            getString(R.string.dialog_text_no_wifi_download_warning)
                        } else {
                            getString(R.string.dialog_text_download_warning)
                        }
                        download_notice_text.text = String.format(message,
                            byteSizeToString(this, model.downloadSize))
                        download_notice_btn_cancel.setOnClickListener {
                            stopService()
                            finish()
                        }
                        download_notice_btn_continue.setOnClickListener {
                            download_notice_collapsible.visibility = View.GONE
                            model.setState(BenchmarkActivityState.DOWNLOAD)
                        }
                    }
                    BenchmarkActivityState.DOWNLOAD -> {
                        resetUiStates()
                        download_collapsible.visibility = View.VISIBLE
                        disableBenchmarkSection()
                        disableUploadSection()
                        download_btn_cancel.setOnClickListener {
                            val intent = Intent(Constants.ACTION_CANCEL_DOWNLOAD)
                            sendBroadcast(intent)
                            stopService()
                            finish()
                        }
                        downloadFiles()
                    }
                    BenchmarkActivityState.CHECK -> {
                        resetUiStates()
                        download_collapsible.visibility = View.VISIBLE
                        download_title.text = getString(R.string.download_check_title)
                        disableBenchmarkSection()
                        disableUploadSection()
                        download_btn_cancel.setOnClickListener {
                            stopService()
                            finish()
                        }
                        downloadFiles()
                    }
                    BenchmarkActivityState.PREVIOUS_BENCH -> {
                        resetUiStates()
                        previous_bench_collapsible.visibility = View.VISIBLE
                        disableDownloadSection()
                        disableUploadSection()
                    }
                    BenchmarkActivityState.BENCHMARK_NOTICE -> {
                        resetUiStates()
                        benchmark_notice_collapsible.visibility = View.VISIBLE
                        val message = if (loopNumber == 1) {
                            getString(R.string.dialog_text_no_touch_warning)
                        } else {
                            getString(R.string.dialog_text_no_touch_warning_3)
                        }
                        benchmark_notice_text.text = message
                        disableDownloadSection()
                        disableUploadSection()
                    }
                    BenchmarkActivityState.BENCHMARK -> {
                        resetUiStates()
                        benchmark_collapsible.visibility = View.VISIBLE
                        disableDownloadSection()
                        disableUploadSection()
                        benchmark_cancel.setOnClickListener {
                            stopService()
                            finish()
                        }
                    }
                    BenchmarkActivityState.UPLOAD_NOTICE -> {
                        resetUiStates()
                        enableUploadButtons()
                        upload_notice_collapsible.visibility = View.VISIBLE
                        disableDownloadSection()
                        disableBenchmarkSection()
                        // For some reason, setting the onClickListeners in askUploadPermission does
                        // not work, so they are set here instead
                        upload_notice_btn_yes.setOnClickListener {
                            if (model.googleConnectionHandler.isConnected) {
                                startActivityForResult(Intent(this, BenchGLActivity::class.java),
                                    Constants.RequestCodes.OPENGL)
                            } else {
                                model.googleConnectionHandler.signIn()
                            }
                        }
                        upload_notice_btn_no.setOnClickListener {
                            model.setState(BenchmarkActivityState.DELETE_SAMPLES)
                        }
                    }
                    BenchmarkActivityState.UPLOAD -> {
                        resetUiStates()
                        upload_notice_collapsible.visibility = View.VISIBLE
                        upload_progress.visibility = View.VISIBLE
                        disableDownloadSection()
                        disableBenchmarkSection()
                        disableUploadButtons()
                    }
                    BenchmarkActivityState.UPLOAD_FAILURE -> {
                        resetUiStates()
                        enableUploadButtons()
                        upload_notice_collapsible.visibility = View.VISIBLE
                        upload_notice_failure.visibility = View.VISIBLE
                        disableDownloadSection()
                        disableBenchmarkSection()
                    }
                    BenchmarkActivityState.UPLOAD_SUCCESS -> {
                        resetUiStates()
                        upload_success_collapsible.visibility = View.VISIBLE
                        disableDownloadSection()
                        disableBenchmarkSection()
                        upload_success_btn_visit.setOnClickListener {
                            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.website_url)))
                            startActivityForResult(browserIntent, Constants.RequestCodes.WEBSITE)
                        }
                        upload_success_btn_not_now.setOnClickListener {
                            askFileDeletion()
                        }
                    }
                    BenchmarkActivityState.DELETE_SAMPLES -> {
                        resetUiStates()
                        delete_samples_collapsible.visibility = View.VISIBLE
                        disableDownloadSection()
                        disableBenchmarkSection()
                        delete_samples_btn_yes.setOnClickListener {
                            StorageManager.deleteDirectory(StorageManager.directory + StorageManager.mediaFolder)
                            startResultPage()
                        }
                        delete_samples_btn_no.setOnClickListener {
                            startResultPage()
                        }
                    }
                }
            }
        })
    }

    override fun onBackPressed() {
        val broadcastIntent = Intent(Constants.ACTION_CANCEL_DOWNLOAD)
        sendBroadcast(broadcastIntent)
        stopService()
        super.onBackPressed()
    }

    override fun onResume() {
        model.googleConnectionHandler = GoogleConnectionHandler.instance
        model.googleConnectionHandler.setGoogleSignInClient(this, this)

        val state = model.state.value
        if (state != null && state == BenchmarkActivityState.DOWNLOAD && service?.percent == 100.0) {
            val context = this
            lifecycleScope.launch {
                withContext(Dispatchers.IO) {
                    val resultList = ResultRepository().getCurrentResultList(context)
                    withContext(Dispatchers.Main) {
                        checkForPreviousBench(resultList)
                    }
                }
            }
        } else if (state == null) {
            val broadcastIntent = Intent(Constants.ACTION_RESET_RUNNING)
            sendBroadcast(broadcastIntent)
            if (!model.googleConnectionHandler.isGooglePlayServicesAvailable(this, false)) {
                model.setState(BenchmarkActivityState.NO_GOOGLE)
            } else {
                model.state.value = BenchmarkActivityState.LOADING
                checkFiles()
            }
        }

        val filter = IntentFilter(Constants.ACTION_DOWNLOAD_FINISHED)
        filter.addAction(Constants.ACTION_SERVICE_ERROR)
        filter.addAction(Constants.ACTION_UPDATE_PROGRESS)
        registerReceiver(br, filter)

        super.onResume()
    }

    override fun onPause() {
        unregisterReceiver(br)
        super.onPause()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putSerializable(STATE_VALUE, model.state.value)
        super.onSaveInstanceState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        model.setState(savedInstanceState?.getSerializable(STATE_VALUE) as BenchmarkActivityState)
        super.onRestoreInstanceState(savedInstanceState, persistentState)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    /**
     * This is just a simple override to get a valid keyEvent to use in the method fakeInput
     * AndroidTV specific hack
     */
    override fun dispatchKeyEvent(event: KeyEvent?): Boolean {
        event?.let {
            if (it.action == KeyEvent.ACTION_UP && keyEvent == null) {
                when (event.keyCode) {
                    KeyEvent.KEYCODE_DPAD_RIGHT,
                    KeyEvent.KEYCODE_MEDIA_FAST_FORWARD,
                    KeyEvent.KEYCODE_DPAD_LEFT,
                    KeyEvent.KEYCODE_MEDIA_REWIND,
                    KeyEvent.KEYCODE_DPAD_UP,
                    KeyEvent.KEYCODE_DPAD_DOWN -> this.keyEvent = event
                }
            }
        }
        return super.dispatchKeyEvent(event)
    }

    /**
     * fakeInput will simulate a user input.
     * On Android TV, the tv will shutdown after some time without user input.
     * Calling this a regular intervals will stop the tv from shutting down.
     */
    override fun fakeInput() {
        if (keyEvent != null) {
            dispatchKeyEvent(keyEvent)
        }
    }

    override fun checkFiles() {
        val context = this
        lifecycleScope.launch(Dispatchers.IO) {
            try {
                model.testController = TestRepository.getTestController(context)
                val error = model.checkSamples()
                if (error != 0) {
                    errorSnackbar(view, error)
                    model.setState(BenchmarkActivityState.ERROR)
                }
            } catch (e: Exception) {
                Log.e(TAG, "checkFiles: Failed to get test list")
                withContext(Dispatchers.Main) {
                    errorSnackbar(view, R.string.snack_error_no_samples)
                    model.setState(BenchmarkActivityState.NO_SAMPLES)
                }
            }
        }
    }

    private fun downloadFiles() {
        val broadcastIntent = Intent(Constants.ACTION_START_DOWNLOAD)
        sendBroadcast(broadcastIntent)
    }

    fun updateDownloadProgress(progress: Double, progressText: String) {
        download_progress_bar.progress = (progress * 10.0).toInt()
        download_text.text = progressText
    }

    private fun checkForPreviousBench(resultList: Array<ArrayList<ResultModel>>) {
        model.resultController = ResultController()
        val index = model.resultController.setResultList(this, resultList, loopNumber)
        if (index != -1) {
            model.setState(BenchmarkActivityState.PREVIOUS_BENCH)
            model.testController.currentIndex = index
            val message: String = if (model.resultController.maxLoop > 1) {
                String.format(resources.getString(
                        R.string.dialog_text_previous_bench_loops),
                        model.resultController.currentLoopIndex,
                        model.resultController.maxLoop,
                        model.testController.currentIndex,
                        model.testController.getTestNumber()
                )
            } else {
                String.format(resources.getString(
                        R.string.dialog_text_previous_bench),
                        model.testController.currentIndex,
                        model.testController.getTestNumber()
                )
            }
            previous_bench_text.text = message
            previous_bench_btn_restart.setOnClickListener {
                model.resultController.reset(this, loopNumber)
                model.testController.reset()
                startBenchmark()
            }
            previous_bench_btn_continue.setOnClickListener {
                startBenchmark()
            }
        } else {
            model.resultController.reset(this, loopNumber)
            startBenchmarkNotice()
        }
    }

    private fun startBenchmarkNotice() {
        model.setState(BenchmarkActivityState.BENCHMARK_NOTICE)
        benchmark_notice_btn_continue.setOnClickListener {
            startBenchmark()
        }
        benchmark_notice_btn_cancel.setOnClickListener {
            stopService()
            finish()
        }
        loopNumber = 0
    }

    override fun startBenchmark() {
        model.setState(BenchmarkActivityState.BENCHMARK)
        super.startBenchmark()
    }

    override fun updateBenchmarkProgress(progress: Double, progressPercent: String, progressText: String, sampleName: String) {
        benchmark_progress_bar.progress = (progress * 10.0).toInt()
        benchmark_text.text = progressPercent
        benchmark_progress_description.text = progressText
        benchmark_sample_name.text = sampleName
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Constants.RequestCodes.OPENGL && data != null) {
            model.prepareBenchmarkUpload(this, data, this::uploadCallback)
        } else if (requestCode == Constants.RequestCodes.GOOGLE_CONNECTION) {
            /* Starts the BenchGLActivity to get gpu information */
            model.googleConnectionHandler = GoogleConnectionHandler.instance
            model.googleConnectionHandler.setGoogleSignInClient(this, this)
            val errStr = model.googleConnectionHandler.handleSignInResult(data)
            if (errStr == null) {
                startActivityForResult(Intent(this, BenchGLActivity::class.java),
                        Constants.RequestCodes.OPENGL)
            } else {
                errorSnackbar(view, R.string.dialog_text_err_google)
                model.setState(BenchmarkActivityState.DELETE_SAMPLES)
                Log.e(TAG, "onActivityResult: failed to log in google: $errStr")
            }
        } else if (requestCode == Constants.RequestCodes.WEBSITE) {
            model.setState(BenchmarkActivityState.DELETE_SAMPLES)
        } else if (requestCode == Constants.RequestCodes.RESULTS) {
            finish()
        }
    }

    override fun askUploadPermission() {
        // This is a temporary hack. For some reason, on the samsung Z flip, the BENCHMARK and
        // UPLOAD_NOTICE states get mixed at the end of the benchmark, as if the upload state was
        // applied at the same time as the benchmark state and end up mixed.
        Handler().postDelayed({
            model.googleConnectionHandler = GoogleConnectionHandler.instance
            model.googleConnectionHandler.setGoogleSignInClient(this, this)
            if (model.googleConnectionHandler.isGooglePlayServicesAvailable(this, false))
                model.setState(BenchmarkActivityState.UPLOAD_NOTICE)
            else
                model.setState(BenchmarkActivityState.DELETE_SAMPLES)

        }, 1000)
    }

    private fun uploadCallback(success: Boolean) {
        if (success) {
            model.setState(BenchmarkActivityState.UPLOAD_SUCCESS)
        } else {
            model.setState(BenchmarkActivityState.UPLOAD_FAILURE)
        }
    }

    private fun askFileDeletion() {
        model.setState(BenchmarkActivityState.DELETE_SAMPLES)
    }

    private fun startResultPage() {
        if (model.resultName == null) {
            errorSnackbar(view, R.string.dialog_text_save_failure)
            return
        }
        val intent = Intent(this, ResultPage::class.java)
        intent.putExtra(Constants.EXTRA_RESULT_PAGE_NAME, model.resultName)
        startActivityForResult(intent, Constants.RequestCodes.RESULTS)
    }

    inner class BenchmarkStepperBroadcastReceiver: BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            intent?.let {
                when (intent.action) {
                    Constants.ACTION_SERVICE_ERROR -> {
                        val errorStringId = intent.getIntExtra(Constants.EXTRA_SERVICE_ERROR, 0)
                        if (errorStringId != 0) {
                            errorSnackbar(view, errorStringId)
                        }
                        model.setState(BenchmarkActivityState.DOWNLOAD_NOTICE)
                    }
                    Constants.ACTION_DOWNLOAD_FINISHED -> {
                        context?.let {
                            lifecycleScope.launch{
                                withContext(Dispatchers.IO) {
                                    val resultList = ResultRepository().getCurrentResultList(context)
                                    withContext(Dispatchers.Main) {
                                        checkForPreviousBench(resultList)
                                    }
                                }
                            }
                        }
                    }
                    Constants.ACTION_UPDATE_PROGRESS -> {
                        val percent = intent.getDoubleExtra(Constants.EXTRA_DOWNLOAD_PERCENT, 0.0)
                        val progressString = intent.getStringExtra(Constants.EXTRA_DOWNLOAD_STRING) ?: ""
                        updateDownloadProgress(percent, progressString)
                    }
                    else -> {}
                }
            }
        }
    }

    companion object {
        @Suppress("UNUSED")
        private val TAG = this::class.java.name
    }
}
