/*
 *****************************************************************************
 * StopNotificationBroadcastReceiver.kt
 *****************************************************************************
 * Copyright © 2020-2021 VLC authors and VideoLAN
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

package org.videolan.vlcbenchmark.benchmark

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import org.videolan.vlcbenchmark.Constants
import org.videolan.vlcbenchmark.SampleTransferService

class StopNotificationBroadcastReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context?, intent: Intent?) {
        if (intent != null && context != null) {
            val i = when (intent.action) {
                Constants.ACTION_STOP_BENCHMARK_SERVICE -> {
                    Intent(context, BenchmarkService::class.java)
                }
                Constants.ACTION_STOP_TRANSFERT_SERVICE -> {
                    Intent(context, SampleTransferService::class.java)
                }
                else -> null
            }
            if (i != null)
                context.stopService(i)
        }
    }

    companion object {
        @Suppress("UNUSED")
        private val TAG = this::class.java.name
    }
}