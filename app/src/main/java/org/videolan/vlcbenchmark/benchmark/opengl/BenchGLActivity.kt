/*
 *****************************************************************************
 * BenchGLActivity.kt
 *****************************************************************************
 * Copyright © 2017 - 2021 VLC authors and VideoLAN
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/
package org.videolan.vlcbenchmark.benchmark.opengl

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

/**
 * Class intented to instanciate an GLSurfaceView
 * to be able to get gpu information
 */
class BenchGLActivity : AppCompatActivity() {
    lateinit var surfaceView: BenchGLSurfaceView
    fun sendGlInfo() {
        val retIntent = Intent()
        retIntent.putExtra("gl_renderer", surfaceView.glRenderer)
        retIntent.putExtra("gl_vendor", surfaceView.glVendor)
        retIntent.putExtra("gl_version", surfaceView.glVersion)
        retIntent.putExtra("gl_extensions", surfaceView.glExtension)
        setResult(RESULT_OK, retIntent)
        finish()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        surfaceView = BenchGLSurfaceView(this)
        this.setContentView(surfaceView)
    }
}