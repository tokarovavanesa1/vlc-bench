/*
 *****************************************************************************
 * TestRepository.kt
 *****************************************************************************
 * Copyright © 2020-2021 VLC authors and VideoLAN
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

package org.videolan.vlcbenchmark.tests

import android.content.Context
import android.util.Log
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import org.videolan.vlcbenchmark.Constants
import org.videolan.vlcbenchmark.R
import org.videolan.vlcbenchmark.tests.types.TestPlayback
import org.videolan.vlcbenchmark.tests.types.TestQuality
import org.videolan.vlcbenchmark.tests.types.TestSpeed
import org.videolan.vlcbenchmark.tools.StorageManager
import org.videolan.vlcbenchmark.tools.StorageManager.delete
import org.videolan.vlcbenchmark.tools.StorageManager.getInternalDirStr
import java.io.BufferedReader
import java.io.File
import java.io.IOException
import java.net.URL

object TestRepository {
    @Suppress("UNUSED")
    private val TAG = this::class.java.name

    private fun getJsonArrayContent(jsonArray: JSONArray, testType: Constants.TestType): ArrayList<Test> {
        val testList = ArrayList<Test>()
        for (i in 0 until jsonArray.length()) {
            val testJson = JSONObject(jsonArray[i].toString())
            if (getInternalDirStr(StorageManager.mediaFolder) == null) {
                return ArrayList()
            }
            val test = when(testType) {
                Constants.TestType.QUALITY -> TestQuality(testJson)
                Constants.TestType.PLAYBACK -> TestPlayback(testJson)
                Constants.TestType.SPEED -> TestSpeed(testJson)
                else -> {
                    return ArrayList()
                }
            }
            testList.add(test)
        }
        return testList
    }

    private fun getTestListFromJson(jsonConfigStr: String) : ArrayList<Test> {
        val testList = ArrayList<Test>()
        try {
            val jsonObject = JSONObject(jsonConfigStr)

            if (jsonObject.has("quality")) {
                testList.addAll(getJsonArrayContent(jsonObject.getJSONArray("quality"), Constants.TestType.QUALITY))
            }
            if (jsonObject.has("playback")) {
                testList.addAll(getJsonArrayContent(jsonObject.getJSONArray("playback"), Constants.TestType.PLAYBACK))
            }
            if (jsonObject.has("speed")) {
                testList.addAll(getJsonArrayContent(jsonObject.getJSONArray("speed"), Constants.TestType.SPEED))
            }

            testList.sortWith(Comparator { o1, o2 ->
                val n1 = o1.sample.name.split("_")[0].toInt()
                val n2 = o2.sample.name.split("_")[0].toInt()
                n1.compareTo(n2)
            })
        } catch (e: JSONException) {
            Log.e(TAG, "getTestListFromJson: Failed to load config json: $e")
        }
        return testList
    }

    private fun getConfigVersion(jsonConfigStr: String) : String {
        val jsonObject = JSONObject(jsonConfigStr)
        var version = "0.0.0"
        if (jsonObject.has("profile")) {
            version = jsonObject.getJSONObject("profile").getString("version")
        } else {
            Log.e(TAG, "getConfigVersion: Failed to get config version")
        }
        return version
    }

    @Throws(IOException::class)
    fun getTestList(context: Context) : ArrayList<Test> {
        return getTestListFromJson(requestConfigFile(context))
    }

    @Throws(IOException::class)
    fun getTestController(context: Context): TestController {
        val configFile = requestConfigFile(context)
        return TestController(getTestListFromJson(configFile), getConfigVersion(configFile))
    }

    @Throws(IOException::class)
    private fun requestConfigFile(context: Context): String {
        val url = URL(context.getString(R.string.config_file_location_url))
        val connection = url.openConnection()
        val input = connection.getInputStream()
        val reader = BufferedReader(input.reader())
        return reader.readText()
    }

    fun deleteFiles(): Boolean {
        val dirPath = getInternalDirStr(StorageManager.jsonFolder)
        if (dirPath == "") {
            Log.e(TAG, "Failed to get folder path")
            return false
        }
        val dir = File(dirPath)
        val files = dir.listFiles()
        files?.let {
            for (file in it) {
                delete(file)
            }
        }

        return true
    }
}