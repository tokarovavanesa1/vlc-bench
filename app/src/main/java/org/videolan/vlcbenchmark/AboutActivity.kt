/*
 *****************************************************************************
 * AboutActivity.kt
 *****************************************************************************
 * Copyright © 2017-2021 VLC authors and VideoLAN
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

package org.videolan.vlcbenchmark

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout

class AboutActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about)
        val tabAbout = resources.getString(R.string.tab_about)
        val tabLicence = resources.getString(R.string.tab_licence)
        val toolbar = findViewById<Toolbar>(R.id.main_toolbar)
        toolbar.title = "v." + BuildConfig.VERSION_NAME
        setSupportActionBar(toolbar)
        if (supportActionBar != null) {
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
        } else {
            onBackPressed()
            return
        }
        val tabLayout = findViewById<TabLayout>(R.id.tabs)
        tabLayout.addTab(tabLayout.newTab().setText(tabAbout))
        tabLayout.addTab(tabLayout.newTab().setText(tabLicence))
        val aboutLayout = findViewById<View>(R.id.layout_about)
        val licenceWebView = findViewById<WebView>(R.id.licence_webview)
        val link = aboutLayout.findViewById<TextView>(R.id.about_link)
        link.setText(R.string.about_link)
        val benchLink = aboutLayout.findViewById<TextView>(R.id.about_bench_link)
        benchLink.setText(R.string.about_bench_link)
        val revisionText = aboutLayout.findViewById<TextView>(R.id.revision)
        val revisionStr = getString(R.string.about_revision) + " " + getString(R.string.build_revision) +
                " ( " + getString(R.string.build_time) + " ) " + BuildConfig.BUILD_TYPE
        revisionText.text = revisionStr
        val compiledText = aboutLayout.findViewById<TextView>(R.id.about_compiled)
        compiledText.setText(R.string.build_host)
        val minVlc = aboutLayout.findViewById<TextView>(R.id.vlc_min_version)
        minVlc.text = String.format(getString(R.string.about_vlc_min), BuildConfig.VLC_VERSION)
        licenceWebView.loadUrl("file:///android_asset/licence.htm")
        licenceWebView.webViewClient = object : WebViewClient() {
            override fun onPageFinished(webView: WebView, url: String) {
                webView.settings.javaScriptEnabled = true
                webView.loadUrl("javascript:(function() {" +
                        "var link = document.getElementById('revision_link');" +
                        "var newLink = link.href.replace('!COMMITID!', '" +
                        getString(R.string.build_revision) + "');" +
                        "link.setAttribute('href', newLink);" +
                        "link.innerText = newLink;" +
                        "})()")
                webView.settings.javaScriptEnabled = false
                super.onPageFinished(webView, url)
            }
        }
        val views = arrayOf(aboutLayout, licenceWebView)
        val titles = arrayOf(tabAbout, tabLicence)
        val viewPager = findViewById<ViewPager>(R.id.pager)
        viewPager.adapter = AboutPagerAdapter(views, titles)
        tabLayout.setupWithViewPager(viewPager)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private inner class AboutPagerAdapter constructor(private val views: Array<View>, private val titles: Array<String>) : PagerAdapter() {
        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            return views[position]
        }

        override fun getPageTitle(position: Int): CharSequence {
            return titles[position]
        }

        override fun isViewFromObject(view: View, viewObject: Any): Boolean {
            return view === viewObject
        }

        override fun getCount(): Int {
            return views.size
        }
    }

    companion object {
        @Suppress("Unused")
        private val TAG = this::class.java.name
    }
}